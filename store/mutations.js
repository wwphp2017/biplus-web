import {
	RECORD_ADDRESS,
	ADD_CART,
	SET_PAYOUT,
    GET_USERINFO,
    SET_REPORT,
    SET_HISTORY,
    SET_FILTER,
    SET_HOME_PAGE,
    SET_PAYIN,
    CENTER_TITLE,
    IS_LOGIN,
    USER_AGENT
} from './mutation-types.js'

import {setStore, getStore} from '../src/components/util/mUtils'

export default {
	
	[ADD_CART](state, {
		order_list,
		category_id,
		item_id,
		food_id,
		name,
		price,
		specs,
		packing_fee,
		sku_id,
		stock
	}) {
		state.orderList =order_list;
		//存入localStorage
		setStore('order_list', state.orderList);
	},
	[GET_USERINFO](state, info) {
        console.log(state, info)
	},
    [SET_PAYOUT](state, info) {
        state.payout =info.type;
    },
    
    [SET_REPORT](state, info) {
        state.report =info.type;
    },
    [SET_HISTORY](state, info) {
        state.history =info.history;
    },
    [SET_FILTER](state, info) {
        state.filter =info.filter;
    },
    [SET_HOME_PAGE](state, info) {
        state.homePage =info.homePage;
    },
    [SET_PAYIN](state, info) {
        state.payin =info.payin;
    },
    [CENTER_TITLE](state, info) {
        state.centerTitle =info.title;
    },
    [IS_LOGIN](state, info) {
        state.isLogin =info.isLogin;
    },
    [USER_AGENT](state, info) {
        state.userAgent =info.userAgent;
    },
}


