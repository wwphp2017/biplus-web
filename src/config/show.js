/**
 * title:标题
 * text：文字
 * style:输入框类型
 * value:初始值
 * re:正则
 * error:输入错误提示
 */
export default {
  show (title, text, style, value, re, error) {
    return new Promise((resolve, reject) => {
      this.$prompt(text, title, {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputType: style,
        inputValue: value,
        inputPattern: re,
        inputErrorMessage: error
      }).then(({
        value
      }) => {
        resolve(value)
      }).catch(() => {
        // eslint-disable-next-line prefer-promise-reject-errors
      })
    })
  }

}
