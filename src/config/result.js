import {
  Message
} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
/**
 * style:提示类型 success 成功  error  失败
 * text:信息
 */
export default {
  alert (style, text) {
    if (style === 'success') {
      Message({
        duration: 2000,
        message: text,
        type: 'success'
      })
    } else if (style === 'error') {
      Message({
        duration: 3000,
        message: text,
        type: 'error'
      })
    }
  }
}
