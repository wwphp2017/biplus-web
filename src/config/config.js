export const PASSWORD_REGEX = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,18}$/
export const PAYWORD_REGEX = /^\d{6}$/
export const ACCOUNT_REGEX = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/
export const NUMBER_REGEX = /^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/
export const PASSWORD_VALIDATOR = '密码为8-18位数字和字母组合'
export const PAYWORD_VALIDATOR = '密码为６位纯数字'
export const ACCOUNT_VALIDATOR = '账号为8-12位数字和字母组合'
export const NUMBER_VALIDATOR = '请填写正数'
