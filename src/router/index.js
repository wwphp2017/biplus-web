import Vue from 'vue'
import Router from 'vue-router'
import home from '../page/home/home'
import allTrade from '../components/trade/allTrade.vue'
import details from '../components/trade/details.vue'
import chart from '../components/chart/chart'
import login from '../page/login/login'
import register from '../page/login/register'
import forget from '../page/login/forget'
import poster from '../page/trade/poster'

import myOrder from '../page/trade/myOrder'
import order from '../page/trade/order'
import over from '../page/trade/over'
import detail from '../page/trade/detail'
import real from '../page/real/real'
import asset from '../page/finance/asset'
import payout from '../page/finance/payout'
import payin from '../page/finance/payin'
import account from '../page/finance/account'
import payinRecord from '../page/finance/payinRecord'
import payoutRecord from '../page/finance/payoutRecord'
import transfer from '../page/finance/transfer'
import apply from '../page/finance/apply'

import set from '../page/my/set'
import action from '../page/action/action'
import record from '../page/action/record'
import center from '../page/helpCenter/center'
import centerDetail from '../page/helpCenter/detail'
import news from '../page/helpCenter/news'
import allow from '../page/sub/allow'
import ios from '../page/sub/ios'
import android from '../page/sub/android'

import cust from '../page/cust/cust'
import phoneVal from '../page/safeCenter/phoneVal'
import make from '../page/make/my'

import safeCenter from '../page/safeCenter/safeCenter'
import emailVal from '../page/safeCenter/emailVal'
import googleVal from '../page/safeCenter/googleVal'
import otcPayment from '../page/safeCenter/otcPayment'
import safeword from '../page/safeCenter/safeword'
import password from '../page/safeCenter/password'
import address from '../page/safeCenter/address'

import upload from '../page/safeCenter/upload'
Vue.use(Router)
export default new Router({
    mode:"hash",
  routes: [
    {
        path: '/my/make',
        name: 'make',
        component: make
      },
    {
        path: '/action',
        name: 'action',
        component: action
      },
      {
        path: '/cust',
        name: 'cust',
        component: cust
      },
      {
        path: '/sub/allow',
        name: 'allow',
        component: allow
      },
      {
        path: '/sub/ios',
        name: 'ios',
        component: ios
      },
      {
        path: '/sub/android',
        name: 'android',
        component: android
      },
      {
        path: '/home',
        name: 'home',
        component: home
      },
      {
        path: '',
        redirect:'/home'
      },
      {
        path: '/action/record',
        name: 'record',
        component: record
      },
      {
        path: '/action/record',
        name: 'record',
        component: record
      },
      {
        path: '/finance/apply',
        name: 'apply',
        component: apply
      },
      {
        path: '/finance/transfer',
        name: 'transfer',
        component: transfer
      },
      {
        path: '/finance/account',
        name: 'account',
        component: account
      },
    {
      path: '/center',
      name: 'center',
      component: center
    },
    {
        path: '/center/news',
        name: 'news',
        component: news
      },
      {
        path: '/center/detail',
        name: 'centerDetail',
        component: centerDetail
      },
      {
          path: '/trade/all',
          name: 'trade',
          component: allTrade
        },
        {
            path: '/trade/details',
            name: 'details',
            component: details
          },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/forget',
      name: 'forget',
      component: forget
    },
      {
        path: '/chart',
        name: 'chart',
        component: chart
      },
      {
        path: '/finance/payin',
        name: 'payin',
        component: payin
      },
      {
        path: '/finance/payout',
        name: 'payout',
        component: payout
      },
      {
        path: '/finance/payoutRecord',
        name: 'payoutRecord',
        component: payoutRecord
      },
      {
        path: '/finance/payinRecord',
        name: 'payinRecord',
        component: payinRecord
      },{
        path: '/order',
        name: 'order',
        component: order
      },
      ,{
        path: '/myOrder',
        name: 'myOrder',
        component: myOrder
      },{
        path: '/detail',
        name: 'detail',
        component: detail
      },
      {
        path: '/over',
        name: 'over',
        component: over
      },
      {
        path: '/login',
        name: 'login',
        component: login
      },
      {
        path: '/poster',
        name: 'poster',
        component: poster
      },
      {
        path: '/real',
        name: 'real',
        component: real
      },
      {
        path: '/finance/asset',
        name: 'asset',
        component: asset
      },
      {
        path: '/my/set',
        name: 'set',
        component: set
      },
      {
        path: '/safeCenter',
        name: 'safeCenter',
        component: safeCenter,
        meta: {
            keepAlive : true 
          }
        },
          { path: "/safeCenter/emailVal",
          name:'emailVal', 
          component: emailVal,
          meta: {
            keepAlive : false 
          },
          },
          { path: "/safeCenter/phoneVal",
          name:'phoneVal', 
          component: phoneVal,
          meta: {
            keepAlive : false 
          },
          },
          {
            path: '/safeCenter/googleVal',
            name: 'googleVal',
            component: googleVal,
            meta: {
                keepAlive : false 
              },
          },
          {
            path: '/safeCenter/otcPayment',
            name: 'otcPayment',
            component: otcPayment,
            meta: {
                keepAlive : false 
              },
          },
          {
            path: '/safeCenter/safeword',
            name: 'safeword',
            component: safeword,
            meta: {
                keepAlive : false 
              },
          },
          {
            path: '/safeCenter/password',
            name: 'password',
            component: password,
            meta: {
                keepAlive : false 
              },
          },
          {
            path: '/safeCenter/address',
            name: 'address',
            component: address,
            meta: {
                keepAlive : false 
              },
          },
          {
            path: '/safeCenter/upload',
            name: 'upload',
            component: upload,
          },
  ]
})
