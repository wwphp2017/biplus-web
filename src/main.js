// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
 import Mint from 'mint-ui';
import 'mint-ui/lib/style.css'
import Meta from 'vue-meta'
import http from './http/axios'
import echarts from 'echarts'
import store from '../store/'

// import "../dist/static/css/my-mint.css";
import "./assets/icon/iconfont.css";

import {
    Toast,
    Picker,Header,
    Popup, Loadmore, Navbar, TabItem,Checklist
  } from 'mint-ui'
  Vue.component(Toast)
  Vue.component(Picker.name, Picker)
  Vue.component(Popup.name, Popup);
Vue.component(Navbar.name, Navbar);
Vue.component(TabItem.name, TabItem);
Vue.prototype.$echarts = echarts
Vue.component(Checklist.name, Checklist);
import { Actionsheet } from 'mint-ui';
Vue.component(Actionsheet.name, Actionsheet);
  Vue.use(Meta)
 Vue.use(Mint);
import { Swipe, SwipeItem } from 'mint-ui';

// 导入极验
require('@/assets/gt.js');
// 绑定到原型
Vue.prototype.$initGeet=initGeetest;
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Loadmore.name, Loadmore);
Vue.config.productionTip = false;


// 跳转后返回顶部
router.afterEach((to,from,next) => {
    window.scrollTo(0,0);
})
Vue.component(Header.name, Header);
/* eslint-disable no-new */
Vue.prototype.$http = http
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
})