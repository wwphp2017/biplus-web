/**
 * @desc 封装通用Api
 * @author wangyong
 * @data 2019/6/21
 */
import axios from "./axios";
const uploadFiles = (options, fn, failFunc) => {
    let formData = new FormData();
    options.params.forEach(element => {
        let a, b;
        [a, b] = element;
        formData.append(a, b);
    });
    axios({
        url: options.url,
        method: "post",
        data: formData,
        headers: {
            "Content-Type": "multipart/form-data;charset=utf-8"
        }
    })
        .then(res => {
            if (fn) fn(res);
        })
        .catch(err => {
            if (failFunc) failFunc(err);
        });
};
export {uploadFiles };
