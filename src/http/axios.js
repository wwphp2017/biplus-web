import axios from 'axios'
import router from "../router";

import qs from 'qs'
import { Indicator,Toast } from 'mint-ui';
let http = axios.create({
  transformRequest: [function (data) {
    return qs.stringify(data)
  }],
  headers: {'content-type': 'application/x-www-form-urlencoded'},
})
// 解决session不一致
axios.defaults.withCredentials = true
// 本地服务器 H
axios.defaults.baseURL = '/api'
 //axios.defaults.baseURL = 'http://161.117.206.84:8888/api'
 //本地服务器 ty
 //axios.defaults.baseURL = 'http://192.168.0.110/api'
 axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

// 正式服务器
 //   axios.defaults.baseURL = 'https://biplus.vip/api'
/* eslint-disable no-new */

axios.defaults.timeout = 20000
// 拦截所有请求
axios.interceptors.request.use(
  request => {
  //request.data.session = sessionStorage.getItem('seesion')
  //   request.data.hash = md5((new Date()).valueOf() + request.data.func)
  if (request.method === "post") {
    let isFileUpdata = request.headers["Content-Type"] && request.headers["Content-Type"].includes("multipart/form-data;");
    if (!isFileUpdata && request.data !== "string") {
        request.data = qs.stringify(request.data);
    }
  }
  if (request.method === "get") {
      request.params = request.data;
  }
  return Promise.resolve(request)
}, error => {
  return Promise.reject(error)
})

//拦截所有相应
axios.interceptors.response.use(response => {
  sessionStorage.setItem('seesion', response.data.session)
//   if (response.data.type === 1) {
//     sessionStorage.removeItem('seesion')
//     router.push('/login')
//   }
    // console.log("...",response.data.code)
    if(response.config.url.search('market/newMarketRefresh')>-1 || response.config.url.search('market/userReportsData')>-1){
        return Promise.resolve(response)
    }
    if(response.data.code==401){
        window.location.hash="/login"
    }
  return Promise.resolve(response)
}, error => {
    if(response.config.url.search('market/okexExchangeRate')>-1){
        Indicator.close();
        return Promise.resolve(response);
    }
    Toast("网络异常");
    Indicator.close();
  if (error.includes('timeout')) { // 判断请求异常信息中是否含有超时timeout字符串
    
    return Promise.reject(error) // reject这个错误信息
  } else {
    return Promise.resolve(error)
  }

})

export default {

  findDate(page, beanName, selectBean, selectExtra, jointQueryRule) {
    page = page === '' ? null : page
    selectBean = selectBean === '' ? null : selectBean
    selectExtra = selectExtra === '' ? null : selectExtra
    jointQueryRule = jointQueryRule === '' ? null : jointQueryRule
    var oldobject = {
      func: 'admin.method.find',
      data: {
        beanName,
        selectBean,
        selectExtra,
        jointQueryRule
      }
    }
    Object.assign(oldobject.data, page)
    return oldobject
  },

  updateDate(beanName, updateBean, selectBean, type) {
    return {
      func: 'admin.method.update',
      data: {
        beanName,
        updateBean,
        selectBean,
        type
      }
    }
  },

  fucDate(func, data) {
    return {
      func,
      data
    }
  },
  get(url, data) {
    var cookie;
      if(localStorage.getItem("cookie")){
        cookie={
            headers: {
                'Authorization':  localStorage.getItem("cookie"),
              }
        }
    }else{
        cookie={}
    }
    return axios.get(url, data,cookie)
  },
  post(url, data) {
    var cookie;
    if(localStorage.getItem("cookie")){
        cookie={
          headers: {
              'Authorization':  localStorage.getItem("cookie"),
            }
      }
  }else{
    cookie={}
  }
    return axios.post(url, data,cookie
    )
  },
  upload(url, data) {
    return axios.post(url, data, {
      headers: {
        'Content-Type': 'multipart/form-data; charset=UTF-8',
      }
    })
  },
  delete(url, data) {
    return axios.delete(url, data)
  },
  put(url, data) {
    return axios.put(url, data)
  }
}
